package pages;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import leaftaps.ReadExcel;

public class Annotation {
	public static ChromeDriver driver;
	@Parameters({"url"})
	@BeforeMethod
	public void startApp(String url) {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get(url);
		
		
		
	}
	
	@AfterMethod
	public void closeApp() {
		driver.close();
	}
	
	@DataProvider(name="getExcelData")
	public String[][] dp() throws IOException {
		String[][] data = ReadExcel.excel();
		return data;
	}

}
