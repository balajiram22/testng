package pages;

public class Page1_LoginPage extends Annotation {
public Page1_LoginPage enterUserName() {
	driver.findElementById("username").sendKeys("Demosalesmanager");
	return this;
}
public Page1_LoginPage enterpassword() {
	driver.findElementById("password").sendKeys("crmsfa");
	return this;
}
public Page2_MyHomePage clickLoginButton() {
	driver.findElementByClassName("decorativeSubmit");
	return new Page2_MyHomePage();
}
}
