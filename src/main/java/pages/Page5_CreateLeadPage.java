package pages;

public class Page5_CreateLeadPage extends Annotation {
public Page5_CreateLeadPage enterCompanyName()
{
	driver.findElementById("createLeadForm_companyName").sendKeys("Testleaf");
	return this;
}
public Page5_CreateLeadPage enterFirstName() {
	driver.findElementById("createLeadForm_firstName").sendKeys("Balaji");
	return this;
}
public Page5_CreateLeadPage enterLastName() {
	driver.findElementById("createLeadForm_lastName").sendKeys("C");
	return this;
}
public Page6_ViewLeadPage clickCreateLeadButton() {
	driver.findElementByClassName("smallSubmit").click();
	return new Page6_ViewLeadPage();
}
}
