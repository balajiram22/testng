package leaftaps;

import org.testng.annotations.Test;

public class TC001_CreateLead extends Annotation{
@Test(dataProvider="getExcelData")
public void createLead(String companyName, String firstName, String lastName) {
	driver.findElementByLinkText("Leads").click();
	driver.findElementByLinkText("Create Lead").click();
	
	driver.findElementById("createLeadForm_companyName").sendKeys(companyName);
	driver.findElementById("createLeadForm_firstName").sendKeys(firstName);
	driver.findElementById("createLeadForm_lastName").sendKeys(lastName);
	driver.findElementByClassName("smallSubmit").click();

}
}
