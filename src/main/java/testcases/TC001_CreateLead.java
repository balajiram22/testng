package testcases;

import org.testng.annotations.Test;

import pages.Annotation;
import pages.Page1_LoginPage;

public class TC001_CreateLead extends Annotation {
@Test
public void createLead()
{
	 new Page1_LoginPage()
	 .enterUserName()
	 .enterpassword()
	 .clickLoginButton()
	 .clickCrmsfa()
	 .clickLeadsLink()
	 .clickCreateLeadLink()
	 .enterCompanyName()
	 .enterFirstName()
	 .enterLastName()
	 .clickCreateLeadButton()
	 .viewLead();
	
}
}
